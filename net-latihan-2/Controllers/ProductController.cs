﻿using Dapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using net_latihan_2.Model;
using net_latihan_2.Repository;
using System.Data;

namespace net_latihan_2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : Controller
    {
        private readonly IJWTAuthManager _authentication;
        public ProductController(IJWTAuthManager authentication)
        {
            _authentication = authentication;
        }

        [HttpGet("ProductList")]
        [Authorize(Roles = "Admin")]
        public IActionResult getProduct()
        {
            var result = _authentication.getProductList<ModelProduct>();

            return Ok(result);
        }

        [HttpPost("Create")]
        [Authorize(Roles = "Admin")]
        public IActionResult Register([System.Web.Http.FromBody] ModelProduct product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("idcompany", product.IDCompany, DbType.String);
            dp_param.Add("idbrand", product.IDBrand, DbType.String);
            dp_param.Add("name", product.Name, DbType.String);
            dp_param.Add("variant", product.Variant, DbType.String);
            dp_param.Add("price", product.Price, DbType.String);
            dp_param.Add("iduser", product.IDUser, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelProduct>("sp_createProduct", dp_param);
            if (result.Code == 200)
            {
                return Ok(new { data = product });
            }

            return BadRequest(result);
        }

        [HttpPut("Update")]
        [Authorize(Roles = "Admin")]
        public IActionResult Update([System.Web.Http.FromBody] ModelProduct product, string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("idcompany", product.IDCompany, DbType.String);
            dp_param.Add("idbrand", product.IDBrand, DbType.String);
            dp_param.Add("name", product.Name, DbType.String);
            dp_param.Add("variant", product.Variant, DbType.String);
            dp_param.Add("price", product.Price, DbType.String);
            dp_param.Add("iduser", product.IDUser, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelCompany>("sp_updateProduct", dp_param);
            if (result.Code == 200)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }
    }
}
