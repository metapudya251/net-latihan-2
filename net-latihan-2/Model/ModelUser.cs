﻿using System.ComponentModel.DataAnnotations;
using System;

namespace net_latihan_2.Model
{
    public class ModelUser
    {
        [Required]
        public int? IDCompany { get; set; }

        [Required]
        public string? Name { get; set; }

        public string? Address { get; set; }

        public string? Telephone { get; set; }

        [Required]
        public string? Email { get; set; }

        [Required]
        public string? Username { get; set; }

        [Required]
        public string? Password { get; set; }

        [Required]
        public string? Role { get; set; }

        public DateTime Date { get; set; } = DateTime.Now;
    }
}
