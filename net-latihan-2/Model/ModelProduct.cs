﻿using System.ComponentModel.DataAnnotations;
using System;

namespace net_latihan_2.Model
{
    public class ModelProduct
    {
        [Required]
        public int? IDCompany { get; set; }

        [Required]
        public int? IDBrand { get; set; }

        [Required]
        public string? Name { get; set; }

        public string? Variant { get; set; }

        public double? Price { get; set; }

        public int? IDUser { get; set; }

        public DateTime Date { get; set; } = DateTime.Now;
    }
}
