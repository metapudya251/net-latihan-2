﻿using System.ComponentModel.DataAnnotations;

namespace net_latihan_2.Model
{
    public class ModelCompany
    {
        [Required]
        public string? Name { get; set; }

        public string? Address { get; set; }

        public string? Telephone { get; set; }
    }
}
