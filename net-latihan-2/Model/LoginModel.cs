﻿using System.ComponentModel.DataAnnotations;

namespace net_latihan_2.Model
{
    public class LoginModel
    {
        [Required]
        public string? Email { get; set; }
        [Required]
        public string? Password { get; set; }
    }
}
