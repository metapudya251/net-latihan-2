﻿using System.ComponentModel.DataAnnotations;

namespace net_latihan_2.Model
{
    public class ModelBrand
    {
        [Required]
        public string? Name { get; set; }

        public string? Address { get; set; }

        public string? Telephone { get; set; }

        public string? Email { get; set; }
    }
}
